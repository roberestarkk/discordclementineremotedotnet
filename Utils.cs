using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Discord;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace discordClementineRemoteDotNet
{
    public enum LogLevel
    {
        Exception = -1,
        Info = 1,
        Release = 0,
        Verbose = 2,
    }
    class Utils
    {
        static LogLevel LoggingLevel = LogLevel.Release;
        static int? LogLevelNameLengthCache = null;
        static int LongestLogLevelName
        {
            get
            {
                if (LogLevelNameLengthCache == null)
                {
                    int len = 0;
                    foreach (var item in Enum.GetNames(typeof(LogLevel)))
                    {
                        if (item.Length > len)
                        {
                            len = item.Length;
                        }
                    }
                    LogLevelNameLengthCache = len;
                }
                return LogLevelNameLengthCache ?? -1;
            }
        }
        public static ManualResetEvent AppRunning = new ManualResetEvent(false);
        public static Task Log(LogMessage msg)
        {
            Console.WriteLine(msg.ToString());
            return Task.CompletedTask;
        }

        public static Task LoadSettingsFromFile(out IConfigurationRoot config)
        {
            Utils.Log(LogLevel.Info, "Beginning load of Discord Token from file");
            if (File.Exists(Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json")))
            {
                Utils.Log(LogLevel.Verbose, "Loading File");
                var builder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json");
                config = builder.Build();
                Utils.Log(LogLevel.Verbose, "File Loaded");
            }
            else
            {
                Utils.Log(LogLevel.Verbose, "File not Found");
                // This is a terrible idea and I apologise
                var sb = new StringBuilder();
                var sw = new StringWriter(sb);

                Utils.Log(LogLevel.Verbose, "Creating File");
                using (var writer = new JsonTextWriter(sw))
                {
                    writer.Formatting = Formatting.Indented;

                    writer.WriteStartObject();
                    writer.WritePropertyName("discordToken");
                    writer.WriteValue("YOURTOKEN");
                    writer.WriteEndObject();
                }
                File.WriteAllText(Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json"), sb.ToString());
                Console.WriteLine(
                    "====================================================================================\n" +
                    "The appsettings.json file was not detected and has been created for you. \n" +
                    "Please edit this file to contain your Discord token so that the integration can work\n" +
                    "====================================================================================");
                // Figure out how to terminate the application from here
                AppRunning.Set();
                throw new Exception("Cannot proceed without appsettings.json being edited");
            }
            return Task.CompletedTask;
        }
        public static string FormatCollection<T>(string delimiter, T[] things)
        {
            var sb = new StringBuilder();
            sb.AppendJoin(delimiter, things);
            return "[" + sb.ToString() + "]";
        }
        public static void Log(LogLevel severity, string format, params object[] args)
        {
            List<object> tmp = new List<object>(args);
            if (LoggingLevel != LogLevel.Verbose)
            {
                var es = tmp.FindAll(x => x is Exception);
                // If you do have an index for an Exception
                foreach (var item in es)
                {
                    var index = tmp.FindIndex(x => x.Equals(item));
                    tmp.Insert(index, ((Exception)item).Message);
                    tmp.Remove(item);
                }
            }
            Log(severity, string.Format(format, tmp.ToArray()));
        }
        public static void Log(LogLevel severity, string output)
        {
            if ((int)severity <= (int)LoggingLevel)
            {
                Console.WriteLine("{0} :: {1,-" + LongestLogLevelName + "}\t {2}",
                    DateTime.Now, severity, output);
            }
        }
    }
}