using System;
using System.Collections.Concurrent;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using Microsoft.Extensions.Configuration;
using Pb.Remote;

namespace discordClementineRemoteDotNet
{
    class Discord
    {
        DiscordSocketClient Client;
        string Game;
        IConfigurationRoot Config;
        BackgroundWorker MyThread;
        BlockingCollection<Message> Pipe;
        public ManualResetEvent Finished;
        public Discord(BackgroundWorker myThread, ref BlockingCollection<Message> pipe)
        {
            MyThread = myThread;
            Pipe = pipe;
            Finished = new ManualResetEvent(false);
        }
        internal async Task RunAsync()
        {
            try
            {
                Utils.Log(LogLevel.Release, "Starting Discord");
                await Utils.LoadSettingsFromFile(out Config);
                Utils.Log(LogLevel.Release, "Discord Config Loaded");
                Client = new DiscordSocketClient();
                Client.Log += Utils.Log;

                Client.Disconnected += HandleDisconnect;

                await Client.LoginAsync(TokenType.User, Config["discordToken"]);
                await Client.StartAsync();
                Utils.Log(LogLevel.Release, "Discord Started");

                while (!MyThread.CancellationPending)
                {
                    if (Client.ConnectionState == ConnectionState.Connected)
                    {
                        Utils.Log(LogLevel.Info, "Looping Discord");
                        HandleMessagesInPipe();
                    }
                }
                Utils.Log(LogLevel.Release, "Stopping Discord");
                // de-register the disconnect re-try
                Client.Disconnected -= HandleDisconnect;
                SetStatusAsync("");
                await Client.StopAsync();
                Utils.Log(LogLevel.Release, "Stopped Discord");
                Finished.Set();
            }
            catch (Exception ex)
            {
                Utils.Log(LogLevel.Exception, "Discord.RunAsync: {0}", ex);
            }
        }

        public void SetStatusAsync(string status)
        {
            var looping = true;
            for (int i = 0; i < 6 && looping; i++)
            {
                Utils.Log(LogLevel.Info, "Trying to set Game");
                var game = Client.CurrentUser.Game.GetValueOrDefault().Name;
                if (game != status && i == 5)
                    Utils.Log(LogLevel.Exception, "Discord is refusing to set the game again");
                else if (game != status)
                {
                    var t = Client.SetGameAsync(status);
                    t.Wait(new TimeSpan(0, 0, 10));
                    Utils.Log(LogLevel.Release, "Setting game to: " + status);
                }
                else
                {
                    looping = false;
                }
            }
        }

        private void HandleMessagesInPipe()
        {
            Message msg;
            if (Pipe.TryTake(out msg, TimeSpan.FromSeconds(10)))
            {
                switch (msg.Type)
                {
                    case MsgType.CurrentMetainfo:
                        Game = msg.ResponseCurrentMetadata.SongMetadata.Title + " by " + msg.ResponseCurrentMetadata.SongMetadata.Artist;
                        SetStatusAsync(Game);
                        break;
                    case MsgType.Play:
                        SetStatusAsync(Game);
                        break;
                    case MsgType.Pause:
                    case MsgType.Stop:
                        SetStatusAsync("");
                        break;
                    default:
                        throw new Exception("Invalid Message found in Pipe");
                }
            }
        }

        private async Task HandleDisconnect(Exception arg)
        {
            await Client.StartAsync();
        }
    }
}